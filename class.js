var Class = function(/*parentClass1, parentClass2, ..., parentClassN, newClassPrototype*/) {
    var prototype = arguments[arguments.length - 1];

    if(prototype === undefined || Class.isClass(prototype)) {
        prototype = {};
    }

    var parentClasses = [];
    for(var i = 0; i < arguments.length; i++) {
        var parentClass = arguments[i];
        if(Class.isClass(parentClass)) {
            parentClasses.push(parentClass);
        }
    }

    var newClass = function() {
        this.__proto__.init.apply(this, arguments);
    };

    for(var i = 0; i < parentClasses.length; i++) {
        var parentClass = parentClasses[i];
        for(var property in parentClass.prototype) {
            if(!prototype.hasOwnProperty(property)) {
                prototype[property] = parentClass.prototype[property];
            }
        }
    }

    prototype.init = prototype.init || function() {};
    prototype._isClass = true;
    prototype._class = newClass;
    prototype._parentClasses = parentClasses;
    newClass.prototype = prototype;

    for(var property in prototype) {
        if(prototype[property])
        newClass[property] = prototype[property];
    }

    newClass.uninitialized = function() {
        return Object.create(prototype);
    };

    newClass.isInstance = function(obj) {
        return Class.isInstance(obj, newClass);
    };

    return newClass;
}

Class.isClass = function(klass) {
    return (typeof(klass) === 'function' && klass._isClass);
};

Class.isInstance = function(obj, isClass) {
    if(obj === null || typeof(obj) !== "object" || !obj._isClass) {
        return false;
    }

    if(isClass === undefined && obj._isClass) {
        return true;
    }

    var classes = [ obj._class ];
    while(classes.length > 0) {
        var theClass = classes.pop();

        if(theClass === isClass) {
            return true;
        }

        for(var i = 0; i < theClass._parentClasses.length; i++) {
            classes.push(theClass._parentClasses[i]);
        }
    }

    return false;
};

module.exports = Class;