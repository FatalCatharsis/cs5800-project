const electron = require('electron');
const ipc = require('electron').ipcMain;
const app = electron.app
const BrowserWindow = electron.BrowserWindow;
const Class = require('./class.js');

let mainWindow

// the next available id of a crosser
let next_id = 0;

// the set of all crossers. This will contain instances of the BridgeCrosser
// class.
let crossers = {};

// This is a boolean variable which holds the value of whether to allow multiple
// units to cross at once as long as there is no one want to cross the opposite
// way that got there first and all the units that want to cross are going in
// the same direction.
var allowMultiple = false;

// this is a helper function to iterate over the members of a javascript object
// if the callback returns false,it acts as a continue;
// if the callback returns true, it acts as a break;
var foreach = function(object, callback) {
    for(var prop in object) {
        if(!object.hasOwnProperty(prop)) continue;
        if(callback(prop, object[prop]))
            break;
    }
}

// This is the base class for messages that can be sent to BridgeCrosser's
var Message = Class({
    init: function() {
        this.type = "NULL";
    }
});

// The request access message takes the id of the person sending it, the
// time that the message was created, and the direction that the BridgeCrosser
// intends to go in. This informs another BridgeCrosser of your intent to
// cross.
var RequestAccess = Class(Message, {
    init: function(sender, time, direction) {
        this.type = "REQUEST_ACCESS";
        this.id = sender;
        this.time = time;
        this.direction = direction;
    }
});

// The give access message takes the id of the person sending it and the
// time it was created. This informs another BridgeCrosser that he may go
// ahead and cross.
var GiveAccess = Class(Message, {
    init: function(sender, time) {
        this.type = "GIVE_ACCESS";
        this.id = sender;
        this.time = time;
    }
});

// Send to all other BridgeCrosser's that I want to cross. Takes my id and
// the direction i want to go: "left" or "right"
function requestToCross(caller, direction) {
    var requestMessage = new RequestAccess(caller, new Date().getTime(), direction);
    foreach(crossers, function(id, crosser) {
        if(crosser.id === caller) return false;
        crosser.pushMessage(requestMessage);
    });
    return requestMessage;
}

// This is a BridgeCrosser. It has two public functions, the exec function and
// a push message function. The exec represents the process that is being called
// continuously, (around every 10ms), and the pushMessage can be invoked to
// tell other bridge crossers of your intent.
var BridgeCrosser = Class({
    // constructor:
    // id is the unique id of this crosser
    // start is the starting position (defaults to 0)
    // speed is the initial speed of this bridge crosser in terms of segments
    //      per second.
    // (note) there are 4 segments, the first loops, crossing the bridge right
    //       the second loop, and crossing the bridge left.
    init: function(id, start, speed) {
        var self = this;
        this.id = id;

        if(typeof start !== "undefined") {
            this.start = start;
        } else {
            this.start = 0;
        }

        if(typeof speed !== "undefined") {
            this.speed = speed;
        } else {
            this.speed = 0;
        }

        this.speed = speed;
        // this is you current position. 0.0 -0.25 is some progress on the first segment
        // 0.25 - 0.5 is progress across the bridge going right
        // 0.5 - 0.75 around the second loops
        // 0.75 - 1.0 is progress across the bridge going left
        this.position = 0.0;

        // queue of all the messages
        this.messages = [];

        // list of all the people whose action I am actively deferring so that
        // I may take my action (fair is fair)
        this.requesters = new Set;

        // list of all the people who have said that i may cross
        this.givers = new Set;

        // If I have expressed an interest in crossing, then my request is not
        // null and equal to the RequestAccess message
        this.currentRequest = null;

        // this is either "left", "right", "left_wait", "right_wait", or empty
        // string. If I'm currently crossing, it is either "left" or "right"
        // If I am waiting to cross, it is either "left_wait" or "right_wait"
        // if I am on a loop, then it is empty string like it is by default.
        this.crossing = "";

        // the last time this function was called.
        this.lastTime = new Date().getTime();

        this.exec = function() {
            // update timing information
            var currentTime = new Date().getTime();
            var dt = (currentTime - self.lastTime)/1000;
            var prev = self.position;
            var newpos = self.position + (dt * self.speed * 0.1);
            self.lastTime = currentTime;

            // helper to determine I have been given the okay to cross by all
            // other individuals in the system
            var canCross = function() {
                var cross = true;
                foreach(crossers, function(id, crosser) {
                    if(crosser.id === self.id) return false;
                    if(!self.givers.has(crosser.id)) {
                        cross = false;
                        return true;
                    }
                });
                return cross;
            };

            // handle any messages if there are any
            for(var message of self.messages) {
                // if the message received was to request access to the bridge
                if(message.type === "REQUEST_ACCESS") {
                    // if I am not currently crossing
                    if(self.crossing === "left_wait" || self.crossing === "right_wait" || self.crossing === "") {
                        // and I am not waiting to cross the bridge myself
                        if(self.currentRequest === null) {
                            // tell them they can cross
                            console.log(self.id + " allow cross of " + message.id);
                            crossers[message.id].pushMessage(new GiveAccess(self.id, new Date().getTime()));
                        } else {
                            // If I am currently waiting, compare timestamps.
                            // if he has been waiting longer, let him through
                            if(message.time < self.currentRequest.time) {
                                console.log(self.id + " allow cross of " + message.id);
                                crossers[message.id].pushMessage(new GiveAccess(self.id, new Date().getTime()));
                            } else {
                                console.log(self.id + " deferring requester " + message.id);
                                self.requesters.add(message.id);
                            }
                        }
                    } else {
                        // If I am allowing multiple, see if the other guy is
                        // crossing in the same direction and tell him I am okay
                        // with him crossing, otherwise defer him.
                        if(allowMultiple) {
                            if(message.direction === self.crossing) {
                                console.log(self.id + " allow cross of " + message.id);
                                crossers[message.id].pushMessage(new GiveAccess(self.id, new Date().getTime()));
                            } else {
                                console.log(self.id + " deferring requester " + message.id);
                                self.requesters.add(message.id);
                            }
                        } else {
                            console.log(self.id + " deferring requester " + message.id);
                            self.requesters.add(message.id);
                        }
                    }
                } else if(message.type === "GIVE_ACCESS") {
                    // someone else has given me access and I am not crossing
                    if(self.crossing === "left_wait" || self.crossing === "right_wait" || self.crossing === "") {
                        // then add the kind sir to the set of people who have
                        // given me their go ahead
                        self.givers.add(message.id);

                        // then if everyone has said yes...
                        if(canCross()) {
                            // empty the set for now...
                            self.givers.clear();
                            // clear out my request...
                            self.currentRequest = null;
                            // and start crossing
                            console.log(self.id + " crossing now");
                            if(self.crossing === "right_wait") {
                                prev = 0.251;
                                newpos = 0.251;
                                self.crossing = "right";
                            } else {
                                prev = 0.751;
                                newpos = 0.751;
                                self.crossing = "left";
                            }

                            // if I am allowing multiple people to cross, check
                            // if anybody is bound in my direction and give them
                            // the okay to travel.
                            // (note) they still might not be aloud to because
                            // somebody got to the other side first
                            if(allowMultiple) {
                                let accessMessage = new GiveAccess(self.id, new Date().getTime());
                                for(var requester of self.requesters) {
                                    let dir = crossers[requester].crossing;
                                    if((dir === "right_wait" && self.crossing === "right") ||
                                       (dir === "left_wait" && self.crossing === "left"))
                                    {
                                        console.log("same direction");
                                        crossers[requester].pushMessage(accessMessage);
                                        self.requesters.delete(requester);
                                        console.log(self.id + " allow cross of " + requester);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // empty out all the messages, since they have been handled
            self.messages = [];

            // if I am not currently waiting on a request to cross...
            if(self.currentRequest === null) {
                // If I have just reached the bridges left side...
                if(0.0 <= prev && prev < 0.25 && 0.25 <= newpos) {
                    console.log(self.id + " asking to cross right.");
                    // ask to cross
                    self.currentRequest = requestToCross(self.id, "right");
                    self.crossing = "right_wait";
                }
                // If I have just reached the end of the bridge going left
                else if(0.25 <= prev && prev < 0.5 && 0.5 <= newpos) {
                    // give access to everyone waiting
                    console.log(self.id + " finished cross right.");
                    self.crossing = "";
                    let accessMessage = new GiveAccess(self.id, new Date().getTime());
                    for(var requester of self.requesters) {
                        crossers[requester].pushMessage(accessMessage);
                        console.log(self.id + " allow cross of " + requester);
                    }
                    self.requesters.clear();
                    self.position = newpos % 1.0;
                }
                // If i have just reached the bridges right side ...
                else if(0.5 <= prev && prev < 0.75 && 0.75 <= newpos) {
                    // ask to cross
                    self.currentRequest = requestToCross(self.id, "left");
                    console.log(self.id + " asking to cross left.");
                    self.crossing = "left_wait";
                }
                // If I have just reached the end of the bridge going left
                else if(0.75 <= prev && prev < 1.0 && 1.0 <= newpos) {
                    // give access to everyone waiting
                    console.log(self.id + " finished crossing left.");
                    self.crossing = "";
                    let accessMessage = new GiveAccess(self.id, new Date().getTime());
                    for(var requester of self.requesters) {
                        crossers[requester].pushMessage(accessMessage);
                        console.log(self.id + " allow cross of " + requester);
                    }
                    self.requesters.clear();
                    self.position = newpos % 1.0;
                } else {
                    // SHOULD never happen, but just in case, try to make progress
                    self.position = newpos % 1.0;
                }
            }
        }

        // add a message to my message queue from someone else
        this.pushMessage = function(message) {
            if(!Class.isInstance(message, Message))
                throw "Expected type \"Message\" received " + typeof message;

            self.messages.push(message);
        }
    }
});

// helper function to create a new crosser
var AddCrosser = function(start_pos, speed) {
    if(typeof start_pos === "undefined")
        var start_pos = 0;
    if(typeof speed === "undefined")
        var speed = 1;
    let id = ++next_id;
    let newCrosser = new BridgeCrosser(id, start_pos, speed);
    crossers[id] = newCrosser;
    // attach interval called back to go off eveyr 10 seconds
    setInterval(newCrosser.exec, 10);
}

function createWindow () {
    mainWindow = new BrowserWindow({width: 800, height: 600, useContentSize: true})

    mainWindow.loadURL('file://' + __dirname + '/index.html')

    // adds the 4 crossers except with
    for(let i = 0; i < 4; i++) {
        AddCrosser(0, (i+1)/2);
    }

    // set callback for when the gui frontend request a redraw
    // pass the current state of all the bridge crossers
    ipc.on('refresh', function(event, arg) {
        event.returnValue = crossers;
    });

    // callbacks from the renderer to get and set values from the gui
    ipc.on('allow-multiple', function(event, arg) {
        event.returnValue = allowMultiple;
    });

    ipc.on('speed1', function(event, arg) {
        event.returnValue = crossers["1"].speed;
    });

    ipc.on('speed2', function(event, arg) {
        event.returnValue = crossers["2"].speed;
    });

    ipc.on('speed3', function(event, arg) {
        event.returnValue = crossers["3"].speed;
    });

    ipc.on('speed4', function(event, arg) {
        event.returnValue = crossers["4"].speed;
    });

    ipc.on('set-allow-multiple', function(event, arg) {
        allowMultiple = arg;
    });

    // helper funtion to check if a speed is valid.
    var validate = function(speed) {
        if(isNaN(speed))
            return false;
        return true;
    }

    ipc.on('set-speed1', function(event, arg) {
        if(validate(arg))
            crossers["1"].speed = arg;
    });

    ipc.on('set-speed2', function(event, arg) {
        if(validate(arg))
            crossers["2"].speed = arg;
    });

    ipc.on('set-speed3', function(event, arg) {
        if(validate(arg))
            crossers["3"].speed = arg;
    });

    ipc.on('set-speed4', function(event, arg) {
        if(validate(arg))
            crossers["4"].speed = arg;
    });

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        mainWindow = null
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})
