// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

var $ = require('jquery');
var ipc = require('electron').ipcRenderer;

function sizeChange(event) {
    var $canvas = $('#mainCanvas');

    var width = window.innerWidth;
    var height = window.innerHeight - 10;

    if((width / height) < 2) {
        height = width / 2;
    } else {
        width = 2 * height;
    }

    $canvas.get(0).width = width;
    $canvas.get(0).height = height;
};

var $canvas = $('#mainCanvas');
var ctx = $canvas[0].getContext('2d');

var foreach = function(object, callback) {
    for(var prop in object) {
        if(!object.hasOwnProperty(prop)) continue;
        if(callback(prop, object[prop]))
            break;
    }
};

var allowMultiple = ipc.sendSync('allow-multiple');
if(allowMultiple){
    $('#allow-multiple').prop('checked', 'true');
}
$('#allow-multiple').change(function() {
    ipc.send('set-allow-multiple', $(this).is(":checked"));
});

var speed1 = ipc.sendSync('speed1');
$('#unit1-speed').val(speed1);
$('#unit1-speed').change(function() {
    ipc.send('set-speed1', $('#unit1-speed').val());
});

var speed2 = ipc.sendSync('speed2');
$('#unit2-speed').val(speed2);
$('#unit2-speed').change(function() {
    ipc.send('set-speed2', $('#unit2-speed').val());
});

var speed3 = ipc.sendSync('speed3');
$('#unit3-speed').val(speed3);
$('#unit3-speed').change(function() {
    ipc.send('set-speed3', $('#unit3-speed').val());
})

var speed4 = ipc.sendSync('speed4');
$('#unit4-speed').val(speed4);
$('#unit4-speed').change(function() {
    ipc.send('set-speed4', $('#unit4-speed').val())
});

function draw() {
    var guiUnitX = $canvas.width()/1000;
    var guiUnitY = $canvas.height()/1000;

    ctx.clearRect(0, 0, $canvas.width(), $canvas.height());

    var x, y, x1, y1, x2, y2;
    var drawLine = function(x1, y1, x2, y2) {
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
    }

    var drawRect = function(centerx, centery, radius) {
        ctx.fillRect(centerx - radius, centery - radius, 2*radius, 2*radius);
    };

    x = guiUnitX * 100;
    y1 = guiUnitY * 200;
    y2 = guiUnitY * 800;
    ctx.strokeStyle = "black";
    drawLine(x, y1, x, y2);

    x1 = x;
    x2 = guiUnitX * 350;
    y = y1;
    drawLine(x1, y, x2, y);

    x = x2;
    y1 = guiUnitY * 200;
    y2 = guiUnitY * 800;
    drawLine(x, y1, x, y2);

    x1 = guiUnitX * 100;
    x2 = guiUnitX * 350;
    y = y2;
    drawLine(x1, y, x2, y);

    x1 = guiUnitX * 350;
    x2 = guiUnitX * 650;
    y = guiUnitY * 500;
    drawLine(x1, y, x2, y);

    x = guiUnitX * 650;
    y1 = guiUnitY * 200;
    y2 = guiUnitY * 800;
    drawLine(x, y1, x, y2);

    x1 = x;
    x2 = guiUnitX * 900;
    y = y1;
    drawLine(x1, y, x2, y);

    x = x2;
    y1 = guiUnitY * 200;
    y2 = guiUnitY * 800;
    drawLine(x, y1, x, y2);

    x1 = guiUnitX * 650;
    x2 = guiUnitX * 900;
    y = y2;
    drawLine(x1, y, x2, y);

    var crossers = ipc.sendSync('refresh');

    ctx.fillStyle ="red";
    // the first 25% is chunked into the first half line segment (1/8),
    // the second line segment which is (1/8) to (3/8)
    // the third line segment which is (3/8) to (5/8)
    // the fourth line segment which is(5/8) to (7/8)
    // and then the second half of the first line segment
    foreach(crossers, function(id, crosser) {
        switch(crosser.id) {
            case 1:
                ctx.fillStyle = "red";
                break;
            case 2:
                ctx.fillStyle = "green";
                break;
            case 3:
                ctx.fillStyle = "blue";
                break;
            case 4:
                ctx.fillStyle = "yellow";
                break;
        }
        if(0 <= crosser.position && crosser.position < 0.25) {
            var chunkProgress = crosser.position * 4;
            if(chunkProgress < 1/8) {
                var segmentProgress = chunkProgress * 8;
                y = 500 - ((800 - 500) * segmentProgress);
                drawRect(350 * guiUnitX, y * guiUnitY, 20 * guiUnitY);
            } else if (1/8 <= chunkProgress && chunkProgress < 3/8) {
                var segmentProgress = ((chunkProgress - (1/8)) * 4);
                x = 350 - ((350 - 100) * segmentProgress);
                drawRect(x * guiUnitX, 200 * guiUnitY, 20 * guiUnitY);
            } else if(3/8 <= chunkProgress && chunkProgress < 5/8) {
                var segmentProgress = ((chunkProgress - (3/8)) * 4);
                y = 200 + ((800 - 200) * segmentProgress);
                drawRect(100 * guiUnitX, y * guiUnitY, 20 * guiUnitY);
            } else if(5/8 <= chunkProgress && chunkProgress < 7/8) {
                var segmentProgress = ((chunkProgress - (5/8)) * 4);
                x = 100 + ((350 - 100) * segmentProgress);
                drawRect(x * guiUnitX, 800 * guiUnitY, 20 * guiUnitY);
            } else {
                var segmentProgress = ((chunkProgress - (7/8)) * 8);
                y = 800 - ((800 - 500) * segmentProgress);
                drawRect(350 * guiUnitX, y * guiUnitY, 20 * guiUnitY);
            }
        } else if(0.25 <= crosser.position && crosser.position < 0.5) {
            var chunkProgress = (crosser.position - (1/4)) * 4;
            x = 350 + ((650 - 350) * chunkProgress);
            drawRect(x * guiUnitX, 500 * guiUnitY, 20 * guiUnitY);
        } else if(0.5 <= crosser.position && crosser.position < 0.75) {
            var chunkProgress = (crosser.position - (1/2)) * 4;
            if(chunkProgress < 1/8) {
                var segmentProgress = chunkProgress * 8;
                y = 500 + ((800 - 500) * segmentProgress);
                drawRect(650 * guiUnitX, y * guiUnitY, 20 * guiUnitY);
            } else if (1/8 <= chunkProgress && chunkProgress < 3/8) {
                var segmentProgress = ((chunkProgress - (1/8)) * 4);
                x = 650 + ((900 - 650) * segmentProgress);
                drawRect(x * guiUnitX, 800 * guiUnitY, 20 * guiUnitY);
            } else if(3/8 <= chunkProgress && chunkProgress < 5/8) {
                var segmentProgress = ((chunkProgress - (3/8)) * 4);
                y = 800 - ((800 - 200) * segmentProgress);
                drawRect(900 * guiUnitX, y * guiUnitY, 20 * guiUnitY);
            } else if(5/8 <= chunkProgress && chunkProgress < 7/8) {
                var segmentProgress = ((chunkProgress - (5/8)) * 4);
                x = 900 - ((900 - 650) * segmentProgress);
                drawRect(x * guiUnitX, 200 * guiUnitY, 20 * guiUnitY);
            } else {
                var segmentProgress = ((chunkProgress - (7/8)) * 8);
                y = 200 + ((500 - 200) * segmentProgress);
                drawRect(650 * guiUnitX, y * guiUnitY, 20 * guiUnitY);
            }
        } else {
            var chunkProgress = (crosser.position - (3/4)) * 4;
            x = 650 - ((650 - 350) * chunkProgress);
            drawRect(x * guiUnitX, 500 * guiUnitY, 20 * guiUnitY);
        }
    });

    window.requestAnimationFrame(draw);
}
window.requestAnimationFrame(draw);


$(window).resize(sizeChange);
sizeChange();